#!/bin/sh

project_url="https://gitlab.com/VladRico/env_autoconf.git"
project_name="env_autoconf"

CONFIGHOME="$HOME/.config"

want_mutt="n"
echo "Do you want mutt installation ? (y/n)"
read -n 1 -r want_mutt

OS=$(uname -s)
CONFIGHOME="${HOME}/.config"

[ "$OS" = "Linux" ] && . /etc/os-release || ID="$OS"

ID="$ID $ID_LIKE"

case $ID in
*debian* | *ubuntu*)
  pkg=apt
  bat=batcat
  sudo=sudo
  tmux=tmux
  git=git
  ;;
*arch*)
  pkg=pacman -S
  bat=bat
  sudo=sudo
  tmux=tmux
  git=git
  ;;
*alpine*)
  pkg=apk
  bat=bat
  sudo=sudo
  tmux=tmux
  git=git
  ;;
*NetBSD*)
  pkg="pkgin -y"
  bat=bat
  sudo=doas
  tmux=tmux
  git=git
  ;;
*FreeBSD*)
  pkg=pkg
  bat=bat
  sudo=doas
  tmux=tmux
  git=git
  ;;
*OpenBSD*)
  pkg=pkg_add
  bat=bat
  sudo=doas
  ;;
*)
  echo "echo unsupported"
  ;;
esac

# commands we want, in the format <command name>:<package name>
FAVCMDS="$bat:bat rg:ripgrep fzf:fzf tmux:tmux"

pkgs=""
for cmd in $FAVCMDS; do
  pathcmd="${cmd%:*}"
  if ! command -v $pathcmd >/dev/null; then
    pkgs="$pkgs ${cmd#*:}"
  fi
done

[ -n "$pkgs" ] && [ $(echo "${ID}" | grep -iq "alpine") ] && $sudo $pkg add $pkgs || $sudo $pkg install $pkgs

## Powerline bash
mkdir -p "$CONFIGHOME"
curl -L "https://gitlab.com/bersace/powerline-bash/raw/master/powerline.bash" \
  -o "$CONFIGHOME/powerline.bash"
chmod u+x "$CONFIGHOME/powerline.bash"

# Get project files
cd /tmp && git clone $project_url

# Bash
if [ -f "$HOME/.bashrc" ]; then
  cp "$HOME/.bashrc" "$HOME/.bashrc.$(date "+%Y.%m.%d-%H.%M.%S").bak"
fi
cp "/tmp/$project_name/bash/bashrc" "$HOME/.bashrc"
cp "/tmp/$project_name/bash/bash_profile" "$HOME/.bash_profile"

# Tmux
mkdir -p "$CONFIGHOME/tmux"
cp "/tmp/$project_name/tmux/_config/tmux.conf" "$CONFIGHOME/tmux/"
cp "/tmp/$project_name/tmux/_config/tmux.remote.conf" "$CONFIGHOME/tmux/"
cp "/tmp/$project_name/tmux/_config/tmux-power.tmux" "$CONFIGHOME/tmux/"
[ ! -f "$HOME/.tmux.conf" ] && ln -s "$CONFIGHOME/tmux/tmux.conf" "$HOME/.tmux.conf"

# Vim
cp "/tmp/$project_name/vim/vimrc" "$HOME/.vimrc"

# Mutt
if [ "$want_mutt" = "y" ]; then
  mkdir -p "$CONFIGHOME/mutt"
  cp "/tmp/$project_name/mutt/_config/muttrc" "$CONFIGHOME/mutt/"
  cp "/tmp/$project_name/mutt/_config/general" "$CONFIGHOME/mutt/"
  cp "/tmp/$project_name/mutt/_config/colors" "$CONFIGHOME/mutt/"
  cp "/tmp/$project_name/mutt/_config/gmail.gpg" "$CONFIGHOME/mutt/"
  [ ! -f "$HOME/.muttrc" ] && ln -s "$CONFIGHOME/mutt/muttrc" "$HOME/.muttrc"
fi

# Clean
rm -rf "/tmp/$project_name"
